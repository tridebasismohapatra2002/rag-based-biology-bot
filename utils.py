from langchain_community.vectorstores import Chroma
from langchain_community.embeddings import GPT4AllEmbeddings

ef = GPT4AllEmbeddings()

persist_directory = 'db'

vectordb = Chroma(persist_directory = persist_directory , embedding_function= ef)

def search_context(query):
    docs = vectordb.similarity_search_with_relevance_scores(query , k = 1)
    content , score = docs[0]
    if score < 0.1:
        return ''
    else:
        return content.page_content