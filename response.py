import os
from langchain_community.llms import HuggingFaceEndpoint
from langchain_core.prompts import ChatPromptTemplate

with open('api_token.txt' , 'r') as file:
    HUGGINGFACEHUB_API_KEY = file.read()


os.environ['HUGGINGFACEHUB_API_KEY'] = HUGGINGFACEHUB_API_KEY


model_id = 'mistralai/Mixtral-8x7B-Instruct-v0.1'
conv_model = HuggingFaceEndpoint(huggingfacehub_api_token = os.environ['HUGGINGFACEHUB_API_KEY'],
                           repo_id = model_id,
                           temperature = 0.6 ,
                           max_new_tokens = 700)

def get_response(query , context):
    prompt = ChatPromptTemplate.from_messages([
        ('system' , """Your name is JARVIS, you are a conversational chatbot responsible to answer only biology based questions according to the context you have been provided with. 
        If the question is not biology based or if you do not know the answer do not try to make up instead just say , 'Sorry I can not help you with that , please ask something else' .
        Please try to answer politely and accurately and in the end always say 'Thanks for asking , Is there anything else I can help you with'."""),
        ('system' , '{context}'),
        ('human' , '{input}')
    ])

    chain = prompt | conv_model

    try:
        res = chain.invoke({'input' : query , 'context' : context})
        res = res.split('JARVIS:')[-1]
    except:
        res = chain.invoke({'input' : query , 'context' : context})
        res = res.split('JARVIS:')[-1]
    return res



