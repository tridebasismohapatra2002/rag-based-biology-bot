import streamlit as st
from utils import search_context
from response import get_response
import time

st.markdown("<h1 style='text-align: center;'>Biology Bot</h1>", unsafe_allow_html=True)

st.markdown("<h3 style='text-align: center;'>Your Virtual Biology Companion!</h3>", unsafe_allow_html=True)

st.write("Meet J.A.R.V.I.S: Your interactive biology companion. Seamlessly engage with biology using AI. Explore, question, and navigate—transforming your queries into conversations and discovery!")
  

if 'messages' not in st.session_state:
    st.session_state.messages = []

for message in st.session_state.messages:
    with st.chat_message(message['role']):
        st.markdown(message['content'])

prompt = st.chat_input()
if prompt:
    with st.chat_message("user"):
        st.markdown(prompt)

    st.session_state.messages.append({'role': 'user' , 'content' : prompt})

    with st.spinner('Generating response...'):
        context = search_context(prompt)
        response = get_response(prompt , context)

    response_container = st.chat_message('assistant')

    response_placeholder = response_container.empty()

    response_text = ""

    for char in response:
        response_text += char
        response_placeholder.markdown(f"<div style='text-align: justify;'>{response_text}</div>", unsafe_allow_html=True)
        time.sleep(0.01)

    st.session_state.messages.append({'role': 'assistant' , 'content' : response})